﻿CKEDITOR.plugins.add('wordimport',
{
	init: function(editor)
	{
		editor.addCommand('wordimport',
		{
			exec: function(editor)
            {
				WordPaster.getInstance().SetEditor(editor).importWord();
			}
		});
		editor.ui.addButton('wordimport',
		{
			label: '导入Word文档',
			command: 'wordimport',
			icon: this.path + 'images/z.png'
		});
	}
});
